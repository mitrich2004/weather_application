package com.example.weather_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        if (intent.getStringExtra("measure") == "imperial") {
            switcher.isChecked = true
            CurrentWeatherActivity.measurementSystem = "imperial"
        } else {
            switcher.isChecked = false
            CurrentWeatherActivity.measurementSystem = "metric"
        }

        ic_back.setOnClickListener {
           finish()
        }

        ic_save.setOnClickListener {
            if (switcher.isChecked) {
                CurrentWeatherActivity.measurementSystem = "imperial"
            } else {
                CurrentWeatherActivity.measurementSystem = "metric"
            }
            val intent = Intent(this, CurrentWeatherActivity::class.java)
            startActivity(intent)
        }
    }
}
