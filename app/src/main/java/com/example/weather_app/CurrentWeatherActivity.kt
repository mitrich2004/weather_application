package com.example.weather_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.InputDevice
import android.view.View
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_current_weather.*
import okhttp3.*
import org.json.JSONObject
import java.io.IOException
import java.math.BigDecimal
import java.math.RoundingMode


class CurrentWeatherActivity : AppCompatActivity() {

    private lateinit var currentTemperature: String
    private lateinit var feelsLike: String
    private lateinit var windSpeed: String
    private lateinit var precipitation: String
    private lateinit var pressure: String

    companion object {
        var measurementSystem: String = "metric"
        var city = "Minsk"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_current_weather)

        ic_refresh.setOnClickListener {
            val reload = Intent(this, CurrentWeatherActivity::class.java)
            startActivity(reload)
        }

        loading_text.visibility = View.VISIBLE
        condition_text.visibility = View.GONE
        condition_image.visibility = View.GONE
        feels_like_text.visibility = View.GONE
        tempreture_text.visibility = View.GONE
        textView4.visibility = View.GONE
        presepitation_text.visibility = View.GONE
        imageView.visibility = View.GONE
        imageView2.visibility = View.GONE
        imageView3.visibility = View.GONE
        imageView4.visibility = View.GONE
        pressure_text.visibility = View.GONE
        humidity_text.visibility = View.GONE
        week_forecast_text.visibility = View.GONE
        wind_text.visibility = View.GONE

        ic_location.setOnClickListener {
            val intent = Intent(this, LocationActivity::class.java)
            startActivity(intent)
        }

        Log.d("TEST", "OK")

        ic_settings.setOnClickListener {
            val intent = Intent(this, SettingsActivity::class.java)
            intent.putExtra("measure", measurementSystem)
            startActivity(intent)
        }

        week_forecast_text.setOnClickListener {
            val intent = Intent(this, WeekWeatherForecast::class.java)
            startActivity(intent)
        }

        val request = Request
            .Builder()
            .url("http://api.apixu.com/v1/current.json?key=7342e0f32399429180873900191808&q=$city")
            .build()

        val client = OkHttpClient()
        client.newCall(request).enqueue(object: Callback{
            override fun onFailure(call: Call, e: IOException) {
                Log.e("ERROR", e.toString())
            }

            override fun onResponse(call: Call, response: Response) {
                val body = response.body!!.string()
                response.close()
                Log.d("TEST", "OK")

                val reader = JSONObject(body)
                val locationReader = reader.getJSONObject("location")
                val currentReader = reader.getJSONObject("current")
                val conditionReader = currentReader.getJSONObject("condition")

                val cityName = locationReader.getString("name")

                if (measurementSystem == "imperial") {
                    currentTemperature = currentReader.getDouble("temp_f").toString()
                    feelsLike = currentReader.getDouble("feelslike_f").toString()
                    windSpeed = currentReader.getDouble("wind_mph").toString()
                    precipitation = currentReader.getDouble("precip_in").toString()
                    pressure = currentReader.getInt("pressure_in").toString()
                } else {
                    currentTemperature = currentReader.getDouble("temp_c").toString()
                    feelsLike = currentReader.getDouble("feelslike_c").toString()
                    windSpeed = (BigDecimal(currentReader.getDouble("wind_kph") / 3.6).setScale(2, RoundingMode.HALF_EVEN)).toString()
                    precipitation = currentReader.getDouble("precip_mm").toString()
                    pressure = currentReader.getInt( "pressure_mb").toString()

                }

                val currentCondition = conditionReader.getString("text")
                val currentConditionImage = conditionReader.getString("icon")
                val humidity = currentReader.getInt("humidity")

                runOnUiThread( Runnable() {
                    run {
                        Picasso.get().load("http:" + currentConditionImage).into(condition_image)
                        city_name.text = cityName
                        condition_text.text = currentCondition
                        humidity_text.text = humidity.toString() + "%"

                        if (measurementSystem == "imperial") {
                            tempreture_text.text = currentTemperature + "°F"
                            feels_like_text.text = "Feels Like " + feelsLike + "°F"
                            wind_text.text = windSpeed + "mph"
                            presepitation_text.text = precipitation + "in"
                            pressure_text.text = pressure + "in"
                        } else {
                            tempreture_text.text = currentTemperature + "°C"
                            feels_like_text.text = "Feels Like " + feelsLike + "°C"
                            wind_text.text = windSpeed + "mps"
                            presepitation_text.text = precipitation + "mm"
                            pressure_text.text = pressure + "mb"
                        }

                        loading_text.visibility = View.GONE
                        condition_text.visibility = View.VISIBLE
                        condition_image.visibility = View.VISIBLE
                        feels_like_text.visibility = View.VISIBLE
                        tempreture_text.visibility = View.VISIBLE
                        textView4.visibility = View.VISIBLE
                        presepitation_text.visibility = View.VISIBLE
                        imageView.visibility = View.VISIBLE
                        imageView2.visibility = View.VISIBLE
                        imageView3.visibility = View.VISIBLE
                        imageView4.visibility = View.VISIBLE
                        pressure_text.visibility = View.VISIBLE
                        humidity_text.visibility = View.VISIBLE
                        week_forecast_text.visibility = View.VISIBLE
                        wind_text.visibility = View.VISIBLE
                    }
                })
            }
        })
    }
}
