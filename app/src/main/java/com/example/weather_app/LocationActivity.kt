package com.example.weather_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_current_weather.view.*
import kotlinx.android.synthetic.main.activity_location.*
import kotlinx.android.synthetic.main.city_row.view.*
import kotlinx.android.synthetic.main.forecast_row.view.*
import kotlinx.android.synthetic.main.forecast_row.view.condition_image
import kotlinx.android.synthetic.main.forecast_row.view.condition_text
import java.math.BigDecimal
import java.math.RoundingMode

class LocationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)

        ic_back.setOnClickListener {
            finish()
        }

        val adapter = GroupAdapter<ViewHolder>()

        cities_recyclerview.adapter = adapter as GroupAdapter

        val citiesList = arrayOf<String>("Minsk", "Amsterdam", "Athens", "Berlin", "Moscow", "Kiev", "Vienna", "Prague",
            "Warsaw", "Madrid", "London", "Lisbon", "Zagreb", "Stockholm", "Copenhagen", "Washington", "New York",
            "Helsinki", "Paris", "Rome", "Monaco", "Bern", "Vilnius", "Riga", "Brussels", "Brest", "Gomel", "Mogilev", "Grodno", "Vitebsk")
            .sortedArray()

        for (city in citiesList) {
            adapter.add(CityItem(citiesList.indexOf(city), city))
        }

        adapter.setOnItemClickListener { item, view ->
            val city = item as CityItem
            CurrentWeatherActivity.city = city.cityName
            val intent = Intent(this, CurrentWeatherActivity::class.java)
            startActivity(intent)
        }

        search.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                adapter.clear()
                for (city in citiesList) {
                    if (city.toLowerCase().contains(p0.toString().toLowerCase())) {
                        adapter.add(CityItem(citiesList.indexOf(city), city))
                    }
                }
            }

        })

    }
}

class CityItem(val color: Int, val cityName: String) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.city_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
       viewHolder.itemView.city_name_text.text = cityName
    }
}
