package com.example.weather_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.JsonReader
import android.util.Log
import android.view.View
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_current_weather.*
import kotlinx.android.synthetic.main.activity_current_weather.view.*
import kotlinx.android.synthetic.main.activity_current_weather.view.condition_image
import kotlinx.android.synthetic.main.activity_current_weather.view.condition_text
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.activity_settings.ic_back
import kotlinx.android.synthetic.main.activity_week_weather_forecast.*
import kotlinx.android.synthetic.main.activity_week_weather_forecast.view.*
import kotlinx.android.synthetic.main.forecast_row.view.*
import okhttp3.*
import org.json.JSONObject
import java.io.IOException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class WeekWeatherForecast : AppCompatActivity() {

    private lateinit var maxTemperature: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_week_weather_forecast)

        ic_back.setOnClickListener {
            finish()
        }

        val adapter = GroupAdapter<ViewHolder>()

        forecast_weather_recyclerview.adapter = adapter as GroupAdapter

        val request = Request
            .Builder()
            .url("http://api.apixu.com/v1/forecast.json?key=7342e0f32399429180873900191808&q=${CurrentWeatherActivity.city}&days=7")
            .build()

        val client = OkHttpClient()
        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("ERROR", "Fail to execute")
            }

            override fun onResponse(call: Call, response: Response) {
                val body = response.body!!.string()
                response.close()
                val reader = JSONObject(body)
                val forecastReader = reader.getJSONObject( "forecast")
                val forecastArray = forecastReader.getJSONArray("forecastday")

                for (i in 0 until forecastArray.length()) {
                    val dateObject = forecastArray.getJSONObject(i)
                    val dayReader = dateObject.getJSONObject("day")
                    val conditionReader = dayReader.getJSONObject("condition")

                    val date = dateObject.getString("date")

                    val conditionText = conditionReader.getString("text")
                    val conditionIcon = conditionReader.getString("icon")

                    runOnUiThread( Runnable() {
                        run {
                            val month = date.substring(5, 7)
                            val day = date.substring(8, date.length)
                            val parsedDate = day + "." + month
                            val dateObject = SimpleDateFormat("yyyy-MM-dd").parse(date)
                            val weekDay = SimpleDateFormat("EE").format(dateObject)
                            if (CurrentWeatherActivity.measurementSystem == "imperial") {
                                adapter.add(ForecastItem(parsedDate, dayReader.getString("maxtemp_f"),  dayReader.getString("mintemp_f"), conditionText, conditionIcon, weekDay))
                            } else {
                                adapter.add(ForecastItem(parsedDate, dayReader.getString("maxtemp_c"), dayReader.getString("mintemp_c"), conditionText, conditionIcon, weekDay))
                            }
                        }
                    })
                }

            }
        })

    }
}

class ForecastItem(val date: String, val maxTemperature: String, val minTemperature: String, val  conditionText: String, val conditionIcon: String, val day: String) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.forecast_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        if (CurrentWeatherActivity.measurementSystem == "imperial") {
            viewHolder.itemView.temperature_text.text = minTemperature + "/" + maxTemperature + "°F"
        } else {
            viewHolder.itemView.temperature_text.text = minTemperature + "/" + maxTemperature + "°C"
        }
        viewHolder.itemView.day_text.text = day
        viewHolder.itemView.condition_text.text = conditionText
        Picasso.get().load("http:" + conditionIcon).into(viewHolder.itemView.condition_image)
        viewHolder.itemView.date_text.text = date

    }
}